# Final Pracitce

users.cv에 이름, 주소, 전화번호를 등록하여 각종 작업을 하는 프로젝트입니다.

## Documentation
### Supported Java Versions

`>=1.7.0`

### More Information
``` kotlin

interface ProfileRegister{
    public void printQuestion(ProfileType profileType); //프로필 타입에 따라서 질문을 보여줌
    public String inputAnswer(ProfileType profileType); //프로필 타입에 따라서 질의를 저장함
}
```

### Execute
`java Main.java`

### Contributing
Please see [CONTRIBUTING.md](https://bitbucket.org/hakzzang/finalpractice/src/master/CONTRIBUTING.md)

### License
Sesame is Free software, and may be redistributed under the terms of specified in the [LICENSE](https://bitbucket.org/hakzzang/finalpractice/src/master/LICENSE) file.
