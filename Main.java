import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.util.Scanner;

public class Main{
     public static void main(String []args){
        ProfileRegister profileRegister = new ProfileRegisterImpl();
        
        profileRegister.printQuestion(ProfileType.NAME);
        profileRegister.inputAnswer(ProfileType.NAME);
        
        profileRegister.printQuestion(ProfileType.EMAIL);
        profileRegister.inputAnswer(ProfileType.EMAIL);
        
        profileRegister.printQuestion(ProfileType.PHONE);
        profileRegister.inputAnswer(ProfileType.PHONE);
    }
}

enum ProfileType{
    NAME, EMAIL, PHONE;    
}

interface ProfileRegister{
    public void printQuestion(ProfileType profileType);
    public String inputAnswer(ProfileType profileType);
}

class ProfileRegisterImpl implements ProfileRegister{
    FileManager fileManager = new FileManagerImpl();

    ProfileRegisterImpl(){
        fileManager.createNewFile();
    }
    
    private BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
    
    @Override
    public void printQuestion(ProfileType profileType){
        switch(profileType){
            case NAME : 
                System.out.println("Input Name: exam) 문OO");        
                break;
            case EMAIL :
                System.out.println("Input Email: exam) mbh0000@naver.com");
                break;
            case PHONE :
                System.out.println("Input Phone: exam) 010-0000-0000");
                break;
        }
    }
    
    @Override
    public String inputAnswer(ProfileType profileType){
        try{
            String inputData = bf.readLine();
            switch(profileType){
                case NAME : 
                    inputData += "\t";        
                    break;
                case EMAIL :
                    inputData += "\t";
                    break;
                case PHONE :
                    inputData += "\n";
                    break;
            }
            fileManager.inputProfile(inputData);
        }catch(IOException exception){
            System.out.println(exception);
        }
        return "";
    }
}

interface FileManager{
    public void createNewFile();
    public void inputProfile(String profile);
}

class FileManagerImpl implements FileManager{
    private String filePath = "users.txt";
    private File userFile = new File(filePath);
    
    @Override
    public void createNewFile(){
        try{
            FileWriter profileWriter = new FileWriter(filePath);
            if (userFile.createNewFile()) {
                System.out.println("File created: " + userFile.getName());
            } else {
                System.out.println("File already exists.");
            }
        }catch(IOException exception){
            System.out.println(exception);
        }
    }
    
    @Override
    public void inputProfile(String profile){
        try {
            String profileFileText = readFile() + profile;
            FileWriter profileWriter = new FileWriter(filePath);
            profileWriter.write(profileFileText);
            System.out.println(profileFileText);

            profileWriter.close();
        } catch (IOException exception) {
            System.out.println(exception);
        }
    }
    
    private String readFile(){
        String readingWord = "";
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            while ((line = br.readLine()) != null) {
                System.out.println("line:" + line);
                readingWord += line;
            }

            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return readingWord;
    }
}
